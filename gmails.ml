open Sys;;
open Unix;;
open Arg;;
open Core;;
open Curl;;
open Str;;
open Sexplib;;
open Sexplib.Std;;

let cache_file="/tmp/gmail_count/cache_new"
let cache_age_max = 30.0
let username = "USER"
let password = "PWD"
let url = "https://" ^ username ^ ":" ^ password ^ "@mail.google.com/mail/feed/atom/";;

type key = string with sexp;;
type value = (int * float) with sexp;;
type cache = (key * value) list with sexp;;

let valid_entry (_,(_,t)) =
  Unix.time () -.t < cache_age_max;;

let persist_cache cache = Sexp.save_mach cache_file (sexp_of_cache cache);;

let load_cache () =
  if Sys.file_exists cache_file then
    Sexp.load_sexp cache_file
      |> cache_of_sexp
      |> List.filter valid_entry
  (* if the file doesn't exist, the cache is empty and we just return the
   * empty list, letting persist_cache create the file when needed *)
  else [];;

let get_cached key cache =
  match List.filter (fun (k,_) -> k = key) cache with
  | [(_,(c,t)) as v] when valid_entry v -> Some c
  | [_] | [] -> None
  | _ -> raise (Failure "cache contains duplicates");;

let cache_add key value cache = (key, (value, Unix.time ())) :: cache

let string_of_url url =
    try let connection = Curl.init () and write_buff = Buffer.create 1763 in
        Curl.set_writefunction connection
                (fun x -> Buffer.add_string write_buff x; String.length x);
        Curl.set_connecttimeout connection 1;
        Curl.set_timeout connection 2;
        Curl.set_url connection url;
        Curl.perform connection;
        Buffer.contents write_buff
    with
    | Curl.CurlException (errcode,_,_) when
        errcode = CURLE_COULDNT_CONNECT || errcode = CURLE_OPERATION_TIMEOUTED
          -> raise (Failure "No connection");;

let get_remote (key : string) : int option =
  let rexp = Str.regexp "<fullcount>\\([0-9]+\\)</fullcount>" in
  let url = url ^ key in
  let html = string_of_url url in
  ignore (Str.search_forward rexp html 0);
  let count = int_of_string (Str.matched_group 1 html) in
  Some count

let get key=
  let cache = load_cache () in
  match get_cached key cache with
  | Some count -> print_int count; print_newline ()
  | None -> match get_remote key with
            | Some count -> cache_add key count cache
                            |> persist_cache ; print_int count; print_newline ()
            | None -> print_endline "NA";;

if Array.length Sys.argv = 1 then get ""
else if Array.length Sys.argv = 3 && Sys.argv.(1) = "-l" then
        get Sys.argv.(2)
else print_endline "usage:\n  gmails [-l gmail-label]";;
