.PHONY: clean
gmails: gmails.ml
	ocamlfind ocamlopt -syntax camlp4o -o gmails -package unix,sexplib,sexplib.syntax,curl,str,core -thread -linkpkg gmails.ml

clean:
	@rm file gmails gmails.cmi gmails.cmx gmails.o gmon.out 2>/dev/null || true
